/**
 * Рассчитывает родительский адрес, убирая нужное число элементов справа
 * @param path - текущий адрес
 * @param depth - сколько элементов справа надо убрать
 * @example
 *  getParentUrl('/test/new') -> '/test'
 */
export function getParentUrl(path: string, depth = 1) {
  let cpath = path;
  const last = cpath.length - 1;
  if (cpath[last] === '/') {
    cpath = path.slice(0, last);
  }
  for (let i = 0; i < depth; i++) {
    let end = cpath.lastIndexOf('/');
    if (end > 0) {
      cpath = cpath.slice(0, end);
    } else {
      cpath = '/';
    }
  }
  return cpath;
}

/**
 * Парсит значение из адресной строки
 * @param query - значение из адресной строки
 */
export function parseQuerySimple<T>(query: T) {
  return query || undefined;
}

/**
 * Преобразует значение в строку для записи в адресную строку
 * @param data - преобразуемое значение
 */
export function encodeQuerySimple<T>(data: T) {
  return data ? String(data) : undefined
}

/**
 * @deprecated use encodeQuerySimple
 */
export const stringifyQuerySimple = encodeQuerySimple;

/**
 * Парсит массив, сохраненный в адресной строке через запятую
 * @param query - значение из адресной строки
 */
export function parseQueryArray(query: string): string[];
export function parseQueryArray(query: undefined|null): null;
export function parseQueryArray(query?: string|null) {
  return query ? query.split(',') : null;
}

/**
 * Представляет массив в виде строки через запятую
 * @param data - массив
 */
export function encodeQueryArray(data: any[]): string;
export function encodeQueryArray(data: null|undefined): undefined;
export function encodeQueryArray(data?: any[]|null) {
  return data ? data.join(',') : undefined;
}
