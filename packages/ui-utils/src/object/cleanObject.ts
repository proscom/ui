/**
 * Объект без undefined полей
 */
export type CleanedObject<Obj extends object> = Pick<
  Obj,
  {
    [Key in keyof Obj]-?: Obj[Key] extends undefined ? never : Key;
  }[keyof Obj]
>;

/**
 * Возвращает новый объект, равный переданному без undefined-значений
 * @param obj - объект
 */
export function cleanObject<Obj extends {}>(obj: Obj): CleanedObject<Obj> {
  const result = {};
  for (const key of Object.keys(obj)) {
    if (obj[key] !== undefined) {
      result[key] = obj[key];
    }
  }
  return result as CleanedObject<Obj>;
}
