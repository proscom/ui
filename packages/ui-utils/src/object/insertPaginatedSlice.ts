/**
 * Вставляет массив в середину другого массива
 * @param data - новые данные
 * @param oldData - старый массив
 * @param page - номер страницы
 * @param perPage - количество элементов на странице
 */
export function insertPaginatedSlice<Data extends any[]>(data: Data, oldData: Data, page: number, perPage: number = data.length) {
  return [
    ...(oldData?.slice(0, page * perPage) || []),
    ...data,
    ...(oldData?.slice((page + 1) * perPage) || [])
  ]
}
