/**
 * Позволяет задать ограничения типа при определении констант
 *
 * @example
 * interface Item {
 *   value: number;
 * }
 *
 * const t = define<{ [key: string]: Item }>()({
 *   key1: { value: 5 },
 *   key2: { other: 10 } // error
 * });
 *
 * // typeof t = { key1: Item, key2: Item }
 */
export function define<Base>() {
  return function _define<T extends Base>(x: T) {
    return x;
  };
}
