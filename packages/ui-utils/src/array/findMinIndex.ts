/**
 * Находит элемент массива, имеющий наименьшую метрику
 * @param array - массив для поиска
 * @param metric - функция, рассчитывающая метрику
 * @returns index - индекс найденного элемента или -1, если массив пустой
 */
export function findMinIndex<T>(array: T[], metric: (item: T, idx: number) => number) {
  let minMetric: number | null = null;
  let minIndex: number | null = null;
  for (let i = 0; i < array.length; i++) {
    const m = metric(array[i], i);
    if (minMetric === null || m < minMetric) {
      minMetric = m;
      minIndex = i;
    }
  }
  return minIndex ?? -1;
}
