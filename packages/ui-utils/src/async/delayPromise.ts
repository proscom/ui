/**
 * Создает промис, который зарезолвится через заданное время
 * @param time - время в миллисекундах
 */
export function delayPromise(time = 0) {
  return new Promise((resolve) => {
    setTimeout(resolve, time);
  });
}
