import { CustomError as TsCustomError } from 'ts-custom-error';

/**
 * Исправляет прототип инстанса класса, расширяющего Error
 *
 * copy-pasted from https://github.com/adriengibrat/ts-custom-error/blob/master/src/utils.ts
 *
 * @param error - инстанс класса
 * @param prototype - целевой прототип
 *
 * @see https://stackoverflow.com/questions/41102060/typescript-extending-error-class
 */
export function fixErrorPrototype(
  error: any,
  prototype: any = error.constructor.prototype
) {
  const setPrototypeOf = Object.setPrototypeOf;
  if (setPrototypeOf) {
    setPrototypeOf(error, prototype);
  } else {
    error.__proto__ = prototype;
  }

  error.name = prototype.name;
}

/**
 * Возвращает представление ошибки в виде простого объекта.
 * Полезно для сериализации в JSON
 *
 * @param error - объект ошибки
 */
export function getErrorFields(error: any) {
  const result: Record<string, unknown> = {};

  for (const prop of Object.getOwnPropertyNames(error)) {
    if (prop !== 'stack') {
      result[prop] = error[prop];
    }
  }

  return result;
}

/**
 * Алиас для getErrorFields
 * @deprecated используйте getErrorFields
 */
export const getJsonError = getErrorFields;

/**
 * Возвращает представление ошибки в виде простого объекта.
 * Полезно для сериализации в JSON.
 * Пытается вызвать error.toJSON()
 *
 * @param error - объект ошибки
 */
export function convertErrorToPlainObject(error: any) {
  if (error.toJSON && typeof error.toJSON === 'function') {
    return error.toJSON();
  }
  return getErrorFields(error);
}

/**
 * Алиас для convertErrorToPlainObject
 * @deprecated используйте convertErrorToPlainObject
 */
export const tryGetJsonError = convertErrorToPlainObject;

/**
 * Базовый класс для ошибок уровня приложения.
 * Расширяйте его, чтобы избежать проблем с прототипом Error.
 *
 * Создавайте классы ошибок для того, чтобы различать разные типы ошибок
 * в коде с помощью instanceof.
 *
 * @see https://stackoverflow.com/questions/41102060/typescript-extending-error-class
 * @see https://github.com/adriengibrat/ts-custom-error
 *
 * @example
 * class UnexpectedResultError extends CustomError {
 *   constructor(public readonly expected: any, public readonly got: any) {
 *     super(`UnexpectedResultError: expected "${JSON.stringify(expected)}, got "${JSON.stringify(got)}"`);
 *   }
 * }
 *
 * fetch('/data')
 *   .then((result) => result.json())
 *   .then((data) => {
 *     if (!data.success) {
 *       throw new UnexpectedExternalApiResultError(true, data.success);
 *     }
 *   })
 *   .catch((error) => {
 *     if (e instanceof UnexpectedResultError) {
 *       toast.error('Неожиданный ответ сервера');
 *     }
 *     console.error(error);
 *   });
 *
 * @example
 * class UnexpectedExternalApiResultError extends UnexpectedResultError {}
 *
 * fetch('/data')
 *   .then((result) => result.json())
 *   .then((data) => {
 *     if (!data.success) {
 *       throw new UnexpectedExternalApiResultError(true, data.success);
 *     }
 *   })
 *   .catch((error) => {
 *     if (e instanceof UnexpectedResultError) {
 *       toast.error('Неожиданный ответ внешнего API');
 *     }
 *     console.error(error);
 *   });
 *
 */
export class CustomError extends TsCustomError {
  constructor(message?: string) {
    super(message);
    // Fixes TypeError when redefining name in derived classes
    Object.defineProperty(this, 'name', {
      value: 'CustomError',
      writable: true
    });
  }

  /**
   * Вызывается автоматически при использовании JSON.stringify
   */
  toJSON() {
    return getErrorFields(this);
  }
}

/**
 * Базовый класс для ошибок, оборачивающих другую ошибку
 *
 * @example
 * class ForeignKeyError extends WrapperError {
 *   constructor(public readonly field: string, originalError: Error) {
 *     super('ForeignKeyError: ' + field, originalError);
 *   }
 * }
 *
 * function executeDbStatement() {
 *   try {
 *     // ...
 *   } catch (e) {
 *     if (e.message === 'foreign key error') {
 *       throw new ForeignKeyError(e.field, e);
 *     }
 *     throw e;
 *   }
 * }
 *
 * try {
 *   executeDbStatement();
 * } catch (e) {
 *   if (e instanceof ForeignKeyError) {
 *     console.log(e.message, e.field, e.stack, e.originalError.stack);
 *     // ...
 *   } else {
 *     // ...
 *   }
 * }
 */
export class WrappedError extends CustomError {
  constructor(message: string, protected readonly originalError: Error) {
    super(message);
  }
}
