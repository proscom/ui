import { EMDASH, QUARTERSP } from '../utf';
import { declinedText } from '../string/declinedText';

/**
 * Форматирует число в русской локали с пробелами-разделителями и запятой.
 *
 * Рекомендуется вместо этой функции использовать Intl.NumberFormat
 *
 * @param number - число
 * @param decimals - количество знаков после запятой
 * @param hideTrailingZeroes - убрать ли крайние правые нули
 */
export function formatNumber(number: number | null | undefined, decimals = 2, hideTrailingZeroes = false) {
  if (number === null || number === undefined) return EMDASH;
  const withTrailingZeroes = Number(number).toFixed(decimals);
  const baseNumber = hideTrailingZeroes
    ? String(parseFloat(withTrailingZeroes))
    : withTrailingZeroes;
  const formattedNum = baseNumber
    .replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1' + QUARTERSP)
    .replace('.', ',');
  if (formattedNum === 'NaN') {
    return EMDASH;
  }
  if (formattedNum === 'Infinity') {
    return EMDASH;
  }
  return formattedNum;
}

/**
 * Единица измерения в русской локали.
 * Если представлена в виде массива, то содержит соответствующие склонения числительных
 *
 * @example [' штука', ' штуки', ' штук']
 */
export type Unit = string | [string, string, string];

/**
 * Форматирует число с единицей измерения в русской локали
 *
 * @param value - число
 * @param unit - единица измерения
 * @param decimals - количество знаков после запятой
 */
export function formatValueUnit(value: number, unit: Unit, decimals = 2) {
  return formatNumber(value, decimals) + declineUnit(value, unit);
}

/**
 * Склоняет единицу измерения
 * @param number - число
 * @param unit - единица измерения
 */
export function declineUnit(number: number, unit: Unit): string {
  if (Array.isArray(unit)) {
    return declinedText(number, unit[0], unit[1], unit[2]);
  }
  return unit;
}

/**
 * Форматирует долю как проценты с одним знаком после запятой
 * @param first - число
 * @param second - число, соответствующее 100 процентам
 */
export const formatPercent = (first: number, second = 1) => {
  return formatValueUnit((first / second) * 100, '%', 1);
};
