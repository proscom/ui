import { formatValueUnit } from './formatNumber';

const byteUnits = [
  ' B',
  ' KB',
  ' MB',
  ' GB',
  ' TB',
  ' PB',
  ' EB',
  ' ZB',
  ' YB'
] as const;

/**
 * Форматирует размер файла, подбирая нужный суффикс
 * @param fileSizeInBytes - размер файла в байтах
 */
export function formatFileSize(fileSizeInBytes: number) {
  let i = 0;
  while (fileSizeInBytes > 1024 && i < byteUnits.length - 1) {
    fileSizeInBytes = fileSizeInBytes / 1024;
    i++;
  }
  return formatValueUnit(Math.max(fileSizeInBytes, 0.1), byteUnits[i], 1);
}
