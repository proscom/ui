export type Optional<T> = T | null | undefined;

export type Extends<Self extends {}, Base extends {}> = Self & Omit<Base, keyof Self>;
