import formatDistance from 'date-fns/locale/ru/_lib/formatDistance';
import formatRelative from 'date-fns/locale/ru/_lib/formatRelative';
import localize from 'date-fns/locale/ru/_lib/localize';
import match from 'date-fns/locale/ru/_lib/match';
import formatLong from 'date-fns/locale/ru/_lib/formatLong';
import buildLocalizeFn from 'date-fns/locale/_lib/buildLocalizeFn';
import { Locale } from 'date-fns';

const monthValues = {
  narrow: ['Я', 'Ф', 'М', 'А', 'М', 'И', 'И', 'А', 'С', 'О', 'Н', 'Д'] as const,
  abbreviated: [
    'Янв.',
    'Фев.',
    'Март',
    'Апр.',
    'Май',
    'Июнь',
    'Июль',
    'Авг.',
    'Сент.',
    'Окт.',
    'Нояб.',
    'Дек.'
  ] as const,
  wide: [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ] as const
};

/**
 * Номинативная русская локаль для date-fns.
 * Позволяет форматировать даты в именительном падеже
 */
const nomRuLocale: Locale & { nominative?: boolean; } = {
  formatDistance,
  formatLong,
  formatRelative,
  localize: {
    ...localize,
    month: buildLocalizeFn({
      values: monthValues,
      defaultWidth: 'wide',
      defaultFormattingWidth: 'wide'
    })
  },
  match,
  options: {
    weekStartsOn: 1,
    firstWeekContainsDate: 1
  },
  nominative: true
};

export default nomRuLocale;
