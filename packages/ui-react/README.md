# `@proscom/ui-react`

Набор реактовых утилит.
См. документацию к утилитам внутри исходников.

## Установка

```
yarn add @proscom/ui-react react
//
npm install --save @proscom/ui-react react
```

## Состав

### Хуки

- [useClickOutside](./src/hooks/useClickOutside.ts)
- [useControlledState](./src/hooks/useControlledState.ts)
- [useCountdown](./src/hooks/useCountdown.ts)
- [useDelegateFocus](./src/hooks/useDelegateFocus.ts)
- [useFactoryRef](./src/hooks/useFactoryRef.ts)
- [useFocus](./src/hooks/useFocus.ts)
- [useImmediateState](./src/hooks/useImmediateState.ts)
- [useLatestCallbackRef](./src/hooks/useLatestCallbackRef.ts)
- [useLatestCallbacksRef](./src/hooks/useLatestCallbacksRef.ts)
- [useLiveInput](./src/hooks/useLiveInput.ts)
- [useMixinClassRef](./src/hooks/useMixinClassRef.ts)
- [useMountedRef](./src/hooks/useMountedRef.ts)
- [usePropsRef](./src/hooks/usePropsRef.ts)
- [useTimeoutRef](./src/hooks/useTimeoutRef.ts)
- [useTodayDate](./src/hooks/useTodayDate.ts)
- [useToggle](./src/hooks/useToggle.ts)

### Утилиты

- [renderSlot](./src/utils/renderSlot.tsx)
- [SingleTimeoutManager](./src/utils/SingleTimeoutManager.ts)
