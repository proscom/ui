import React from 'react';

export type RenderFunction<Props> = (props: Props) => React.ReactNode;
export type Slot<Props> = React.ReactNode | RenderFunction<Props>;

export function renderSlot<
  Props extends { [key: string]: any } = { [key: string]: any }
>(
  slot: Slot<Props> | undefined,
  props: Props,
  slotComponent?: React.ElementType
): React.ReactNode {
  if (slotComponent) {
    const Component = slotComponent;
    return <Component {...props} />;
  } else if (typeof slot === 'function') {
    return slot(props);
  } else {
    return slot;
  }
}
