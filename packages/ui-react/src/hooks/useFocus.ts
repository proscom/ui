import { FocusEvent, FocusEventHandler, useState } from 'react';
import { useFactoryRef } from './useFactoryRef';
import { usePropsRef } from './usePropsRef';

export interface UseFocusProps<Element> {
  onFocus?: FocusEventHandler<Element>;
  onBlur?: FocusEventHandler<Element>;
}

/**
 * Позволяет использовать focused как состояние компонента
 * Активируется только для focus-visible
 * @param props - параметры хука
 * @param props.onFocus - внешний обработчик onFocus
 * @param props.onBlur - внешний обработчик onBlur
 * @returns
 *   - focused - состояние фокуса (true если :focus-visible)
 *   - onFocus, onBlur - обработчики, которые надо повесить на элемент
 */
export function useFocus<Element extends HTMLElement>({
  onFocus,
  onBlur
}: UseFocusProps<Element> = {}) {
  const [focused, setFocused] = useState(false);

  const propsRef = usePropsRef({
    onFocus,
    onBlur
  });

  const funcsRef = useFactoryRef(() => ({
    onFocus: (e: FocusEvent<Element>) => {
      if (e.target.classList.contains('focus-visible')) {
        setFocused(true);
      }
      propsRef.onFocus?.(e);
    },
    onBlur: (e: FocusEvent<Element>) => {
      setFocused(false);
      propsRef.onBlur?.(e);
    }
  }));

  return {
    focused,
    onFocus: funcsRef.onFocus,
    onBlur: funcsRef.onBlur
  };
}

