import { useLayoutEffect, useRef } from 'react';

/**
 * Возвращает реф, значением которого является true, если компонент еще жив,
 * и false, если он уже размонтирован.
 * Полезно для обработки асинхронных запросов
 *
 * @example
 *  const [state, setState] = useState();
 *  const mountedRef = useMountedRef();
 *  const run = useCallback(() => {
 *    fetch('url').then((result) => {
 *      if (!mountedRef.current) return;
 *      setState(result);
 *    });
 *  }, []);
 */
export function useMountedRef() {
  const ref = useRef<boolean>(true);
  useLayoutEffect(() => {
    ref.current = true;
    return () => {
      ref.current = false;
    };
  }, []);
  return ref;
}
