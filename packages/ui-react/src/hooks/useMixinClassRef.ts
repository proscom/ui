import { useEffect } from 'react';
import { useFactoryRef } from './useFactoryRef';

export interface MixinClass {
  onDidMount?: () => void;
  onWillUnmount?: () => void;
}

/**
 * Позволяет добавить в компонент императивную логику.
 * Чем-то похоже на интеграцию классового компонента
 * в функциональный
 *
 * При маунте компонента будет вызвана функция factory.
 * Ожидается, что она создаст объект. Этот объект
 * сохраняется в useRef и будет постоянен на протяжении
 * всей жизни компонента.
 *
 * Если у объекта есть методы onDidMount и onWillUnmount
 * то они будут вызваны после первого рендера и при
 * анмаунте компонента соответственно.
 *
 * У объекта могут быть дополнительные функции, которые
 * можно вызывать снаружи с помощью instanceRef.current.
 * Например, можно вызывать их в useEffect, и прокидывать
 * обновления данных.
 *
 * @param factory {Function} - функция, создающая объект,
 *  реализующий императивную логику
 */
export function useMixinClassRef<T extends MixinClass>(factory: () => T) {
  const instanceRef = useFactoryRef(factory);

  useEffect(() => {
    const instance = instanceRef;
    instance.onDidMount?.();
    return () => {
      instance.onWillUnmount?.();
    };
  }, [instanceRef]);

  return instanceRef;
}
